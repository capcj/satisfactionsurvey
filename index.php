<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<?php 
require_once 'vendor/autoload.php';
$objSatisfactionSurvey = new Classes\SatisfactionSurvey($_REQUEST['lang'] ?? 'us');
$objParser = new Classes\Parser([
    'js' => [
        'src' => [
            "chartist/dist/chartist.min",
            "sorttable/sorttable"
        ]
    ],
    'css' => [
        'src' => ["chartist/dist/chartist.min"]
    ]
], []);
?>
<html>
    <head>
        <meta charset="UTF-8">
        <title><?= $_SESSION['lang']['project']['name']; ?></title>
        <link rel="stylesheet" href="css/style.css">
        <link rel="stylesheet" href="vendor/components/font-awesome/css/font-awesome.min.css">
        <?= $objParser->parseStyles(); ?>
        <?= $objParser->parseScripts(); ?>
        <script src="js/script.js" defer></script>
    </head>
    <body>
        <?php $objSatisfactionSurvey->renderView(); ?>
    </body>
</html>
