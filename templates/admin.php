<!--
/* 
 * Copyright (C) 2017 ca
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
-->
<a href="<?= htmlspecialchars($_SERVER['SCRIPT_NAME']) . '?lang=' . ($_REQUEST['lang'] ?? 'us'); ?>">
    &larr;<?= $_SESSION['lang']['phrases']['backtosurvey']; ?>
</a>
<h1><?= $_SESSION['lang']['phrases']['welcomeadminpage']; ?></h1>
<form name="surveyresult" method="POST" action="?view=1">
    <label>
        <input type="date" name="period[]" value="<?= ($_POST['period'][0] ?? date('Y-m-d')); ?>">
        <span><?= $_SESSION['lang']['phrases']['to']; ?></span>
        <input type="date" name="period[]" value="<?= ($_POST['period'][1] ?? date('Y-m-d')); ?>">
        <input type="hidden" name="lang" value="<?= $_REQUEST['lang'] ?? 'us'; ?>">
        <input type="submit" name="action" value="<?= $_SESSION['lang']['phrases']['search']; ?>">
    </label>
</form>
<?php
$objReport = new Classes\Report(
        $objSatisfactionSurvey->getSurvey(($_POST['period'] ?? [])), 
        [
            0 => $_SESSION['lang']['options']['dislike'],
            1 => $_SESSION['lang']['options']['like'],
            'tabletitle' => $_SESSION['lang']['phrases']['satisfactionsurvey'],
            'Level' => $_SESSION['lang']['phrases']['satisfied'],
            'Created At' => $_SESSION['lang']['phrases']['filledat']
        ]
);
?>
<h2><?= $_SESSION['lang']['phrases']['generalsatisfaction']; ?></h2>
<div class="ct-chart"></div>
<script>
    window.addEventListener('load', function () {
        <?php if (count($objReport->getKeys()) > 0): ?>
        var labels = <?= json_encode([
              $_SESSION['lang']['options']['dislike'], 
              $_SESSION['lang']['options']['like']
          ]);?>;
                  
        var data = {
          series: <?= json_encode($objReport->getGeneralSatisfaction(true)); ?>
        };

        var sum = function(a, b) { return a.value + b.value };

        new Chartist.Pie('.ct-chart', data, {
          labelInterpolationFnc: function(value, key) {
            return labels[key] + ': ' + Math.round(value / data.series.reduce(sum) * 100) + '%';
          }
        });
        <?php endif; ?>
    }, false);
</script>
<?= $objReport->showTable(); ?>