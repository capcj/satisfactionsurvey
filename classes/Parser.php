<?php

/*
 * Copyright (C) 2017 ca
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Classes;

/**
 * Class created to parse any value needed in the project
 *
 * @author ca
 */
class Parser {
    private $data;
    private $translate;
    
    public function __construct(array $data, array $translate) 
    {
        $this->data = $data;
        $this->translate = $translate;
    }
    
    public function parseKeys(): array
    {
        
        return (count($this->data) > 0) ? array_map(function($k){
            if (is_string($k)) {
                $parsedKey = ucwords(str_replace('_', ' ', $k));
                return ($this->translate[$parsedKey] ?? $parsedKey);
            }
        }, array_keys($this->data[0])) : [];
    }
    
    public function parseValues(): array
    {
        foreach ($this->data as &$row) {
            foreach ($row as $key => &$column) {
                if (!is_numeric($key)) {
                    if (in_array($key, ['created_at', 'modified_at'])) {
                        $column = date('d/m/Y H:m:s', strtotime($column));
                    } else {
                        $column = ($this->translate[$column] ?? $column);
                    }
                } else {
                    unset($row[$key]);
                }
            }
        }
        return $this->data;
    }
    
    public function parseScripts(): string
    {
        $dir = $this->data['js']['dir'] ?? 'node_modules';
        foreach($this->data['js']['src'] as &$data) {
            $data = "<script src='$dir/$data.js' defer></script>";
        }
        return implode(" ", $this->data['js']['src']);
    }
    
    public function parseStyles(): string
    {
        $dir = $this->data['css']['dir'] ?? 'node_modules';
        foreach($this->data['css']['src'] as &$data) {
            $data = "<link rel='stylesheet' href='$dir/$data.css'>";
        }
        return implode(" ", $this->data['css']['src']);
    }
}
