<?php

/*
 * Copyright (C) 2017 ca
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Classes;

/**
 * Description of Report
 *
 * @author ca
 */
class Report 
{
    private $objParser;
    private $keys;
    private $values;
    private $originalData;
    private $output;
    public function __construct(array $data, array $translate)
    {
        $this->originalData = $data;
        $this->objParser = new \Classes\Parser($data, $translate);
        $this->keys = $this->objParser->parseKeys();
        $this->values = $this->objParser->parseValues();
        $this->output = '';
    }
    
    public function getKeys(): array
    {
        return $this->keys;
    }
    
    public function getValues(): array
    {
        return $this->values;
    }
    
    public function getGeneralSatisfaction(bool $withClassNames): array {
        $result = [0,0];
        foreach ($this->values as $opinion) {
            if ($opinion['level'] == $_SESSION['lang']['options']['like']) {
                $result[1]++;
            } else {
                $result[0]++;
            }
        }
        return ($withClassNames) ? [
            [
                'value' => $result[0],
                'className' => 'pie-dislike'
            ],
            [
                'value' => $result[1],
                'className' => 'pie-like'
            ]
        ] : $result;
    }
    
    private function head(): Report
    {
        if (count($this->keys) == 0) {
            return $this;
        }
        $timestamp = time();
        $this->output .= "<table name='survey{$timestamp}' class='sortable' id='survey{$timestamp}'>
             <caption>". 
                ($translate['tabletitle'] ?? 
                $_SESSION['lang']['phrases']['survey']) . 
            "</caption>
             <thead>";
        foreach ($this->keys as $key) {
            $this->output .= "<th>$key</th>";
        }
        $this->output .= "</thead>";
        
        return $this;
    }
    
    private function body(): Report
    {
        if (count($this->keys) == 0) {
            return $this;
        }
        $this->output .= "<tbody>";
        foreach ($this->values as $row) {
            $this->output .= "<tr>";
            foreach ($row as $key => $column) {
                $this->output .= "<td>$column</td>";
            }
            $this->output .= "</tr>";
        }
        $this->output .= "</tbody>";
        
        return $this;
    }
    
    private function foot(): Report
    {
        if (count($this->keys) == 0) {
            return $this;
        }
        $this->output .= "</table>";
        
        return $this;
    }
    
    private function show(): void
    {
        if (count($this->keys) > 0) {
            echo $this->output;
        } else {
            echo "<h2>{$_SESSION['lang']['phrases']['noregisters']}</h2>";
        }
    }
    
    public function showTable(): void
    {
        try {
            $this->head()->body()->foot()->show();
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
    }
}
